# Robot ROS Demos

Example robot model and code for interfacing Gazebo with ROS and controling with joypad.

## Quick Start

Rviz:

    roslaunch robot_description rviz.launch

Gazebo:

    roslaunch robot_gazebo spawn.launch
    

ROS Control:

    roslaunch robot_description robot_control.launch

